package com.organization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class OrganizationApplication {
	
	public static void main(String[] args) {
	
	SpringApplication.run(OrganizationApplication.class , args);
	}

}
