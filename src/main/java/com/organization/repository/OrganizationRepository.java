package com.organization.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.organization.model.OrganizationModel;

public interface OrganizationRepository extends JpaRepository<OrganizationModel , Long> {

}
