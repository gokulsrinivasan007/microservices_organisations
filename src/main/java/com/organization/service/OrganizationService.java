package com.organization.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.organization.model.OrganizationModel;
import com.organization.repository.OrganizationRepository;

@Service
public class OrganizationService {
	
	@Autowired
	OrganizationRepository organizationRepository;
	
	
	public OrganizationModel save(OrganizationModel org) {
		return organizationRepository.save(org);
	}

	
	public List<OrganizationModel> findAll(){
		return organizationRepository.findAll();
	}
	
	
	public OrganizationModel findOne(Long orgid) {
		return organizationRepository.findOne(orgid);
	}
	
	
	public void delete(OrganizationModel org) {
		organizationRepository.delete(org);
	}

}
