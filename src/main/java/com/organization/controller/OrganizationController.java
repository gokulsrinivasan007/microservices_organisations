package com.organization.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.organization.model.OrganizationModel;
import com.organization.service.OrganizationService;

@RestController
@RequestMapping("/org_details")
public class OrganizationController {
	
	@Autowired
	OrganizationService organizationService;
	
	@PostMapping("/org")
	public OrganizationModel createOrg(@Valid @RequestBody OrganizationModel org) {
		return organizationService.save(org);
		
	}
	
	@GetMapping("/org")
	public List<OrganizationModel> getAllOrg(){
		return organizationService.findAll();
	}
	
	@GetMapping("/org/{id}")
	public ResponseEntity<OrganizationModel> getOrgById(@PathVariable(value="id") Long orgid){
		OrganizationModel org = organizationService.findOne(orgid);	
		if(org==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(org);
		
	}
	@PutMapping("/org/{id}")
	public ResponseEntity<OrganizationModel> updateOrg(@PathVariable(value="id") Long orgid,@Valid @RequestBody OrganizationModel orgDetails){
		
		OrganizationModel org=organizationService.findOne(orgid);
		if(org==null) {
			return ResponseEntity.notFound().build();
		}
		
		org.setOrg_name(orgDetails.getOrg_name());
		org.setOrg_address(orgDetails.getOrg_address());
		
		
		OrganizationModel updateOrg=organizationService.save(org);
		return ResponseEntity.ok().body(updateOrg);
	
	}
	@DeleteMapping("/org/{id}")
	public ResponseEntity<OrganizationModel> deleteOrg(@PathVariable(value="id") Long orgid){
		
		OrganizationModel org=organizationService.findOne(orgid);
		if(org==null) {
			return ResponseEntity.notFound().build();
		}
		organizationService.delete(org);
		
		return ResponseEntity.ok().build();
		
		
	}

}
